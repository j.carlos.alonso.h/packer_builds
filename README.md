# Packer Templates

### Introduction

This repository contains different OS templates that can be used to create boxes for vagrant using Packer ([Website](http://www.packer.io))

This contains the next packer files:

* `windows2012_r2.json`. Please download iso ([Evaluate Windows Server 2012 R2](https://www.microsoft.com/es-es/evalcenter/evaluate-windows-server-2012-r2)) into `iso/`

### Packer version

[Packer](https://github.com/mitchellh/packer/blob/master/CHANGELOG.md) `1.1.0` or greater is required.
